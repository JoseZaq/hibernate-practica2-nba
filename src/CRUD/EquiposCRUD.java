package CRUD;

import model.EquiposEntity;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class EquiposCRUD {
    private static Session session;

    public static List<EquiposEntity> getAllSortByName(){
        CriteriaBuilder crb = session.getCriteriaBuilder();
        CriteriaQuery<EquiposEntity> crq = crb.createQuery(EquiposEntity.class);
        Root<EquiposEntity> root = crq.from(EquiposEntity.class);
        crq.orderBy(crb.asc(root.get("nombre")));
        return session.createQuery(crq).list();
    }

    // setters and  getters
    public static void setSession(Session session) {
        EquiposCRUD.session = session;
    }
}

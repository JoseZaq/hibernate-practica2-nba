package CRUD;

import model.EstadisticasEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class EstadisticasCRUD {
    public static Session session;

    public static void insertEstadistica(EstadisticasEntity estadisticas){
        if(JugadoresCRUD.get(estadisticas.getJugadoresByJugador().getCodigo()) == null){
            System.out.println("El jugador no existe en la base de datos. Abortando...");
            return;
        }
        List<EstadisticasEntity> _estadisticas = getByColumns(estadisticas);
        if (_estadisticas != null && _estadisticas.size() != 0) {
                System.out.println("Esta estadistica ya existe.Abortando...");
                return;
        }

        Transaction tx = session.beginTransaction();
        estadisticas.setId(obtenirMaxID()+1);

        session.save(estadisticas);
        tx.commit();
        System.out.println("Estadistica insertada correctamente...\n"+estadisticas);
    }

    public static void getEstadisticas(){
        CriteriaBuilder crb = session.getCriteriaBuilder();
        CriteriaQuery<EstadisticasEntity> crq = crb.createQuery(EstadisticasEntity.class);
        Root<EstadisticasEntity> root = crq.from(EstadisticasEntity.class);
    }

    public static List<EstadisticasEntity> getByColumns(EstadisticasEntity estadistica){
        CriteriaBuilder crb = session.getCriteriaBuilder();
        CriteriaQuery<EstadisticasEntity> crq = crb.createQuery(EstadisticasEntity.class);
        Root<EstadisticasEntity> root = crq.from(EstadisticasEntity.class);

        crq.where(crb.equal(root.get("temporada"),estadistica.getTemporada()),
                crb.equal(root.get("puntosPorPartido"),estadistica.getPuntosPorPartido()),
                crb.equal(root.get("asistenciasPorPartido"),estadistica.getAsistenciasPorPartido()),
                crb.equal(root.get("taponesPorPartido"),estadistica.getTaponesPorPartido()),
                crb.equal(root.get("rebotesPorPartido"),estadistica.getRebotesPorPartido()),
                crb.equal(root.get("jugadoresByJugador").get("codigo"),estadistica.getJugadoresByJugador().getCodigo()));
        if (session.createQuery(crq) == null) {
            return null;
        }
        return session.createQuery(crq).getResultList();
    }

    public static List getBestPointsPerSeason(){
        CriteriaBuilder crb = session.getCriteriaBuilder();
        CriteriaQuery<Object[]> crq = crb.createQuery(Object[].class);
        Root<EstadisticasEntity> root = crq.from(EstadisticasEntity.class);
        crq.multiselect(root.get("temporada"),root.get("jugadoresByJugador").get("codigo"),
                root.get("jugadoresByJugador").get("nombre"),
                root.get("jugadoresByJugador").get("equiposByNombreEquipo").get("nombre"),
                crb.max(root.get("puntosPorPartido")));
        crq.groupBy(root.get("temporada"),root.get("jugadoresByJugador").get("codigo"),
                root.get("jugadoresByJugador").get("nombre"),
                root.get("jugadoresByJugador").get("equiposByNombreEquipo"));

        return session.createQuery(crq).list();
    }

    public static int obtenirMaxID() {
        Query q = session.createQuery("select max(e.id) from EstadisticasEntity as e");
        Object res = q.getSingleResult();
        int max = (int)res;
        return max;
    }
    // getters and setters
    public static void setSession(Session session) {
        EstadisticasCRUD.session = session;
    }

}

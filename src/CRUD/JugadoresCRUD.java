package CRUD;

import model.EstadisticasEntity;
import model.JugadoresEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

public class JugadoresCRUD {
    public static Session session;

    public static void addEstadisticas(EstadisticasEntity estadisticas){
        Transaction tx = session.beginTransaction();

        session.save(estadisticas);
        tx.commit();
        session.close();
    }
    public static JugadoresEntity get(int codigo){
        CriteriaBuilder crb = session.getCriteriaBuilder();
        CriteriaQuery<JugadoresEntity> crq = crb.createQuery(JugadoresEntity.class);
        Root<JugadoresEntity> root = crq.from(JugadoresEntity.class);

        crq.where(crb.equal(root.get("codigo"),codigo));
        //
        return session.createQuery(crq).getSingleResult();
    }

    public static void updateWeight(int codigo, int peso){
        CriteriaBuilder crb = session.getCriteriaBuilder();
        CriteriaUpdate<JugadoresEntity> update = crb.createCriteriaUpdate(JugadoresEntity.class);
        Root<JugadoresEntity> root = update.from(JugadoresEntity.class);
        update.set("peso",peso);
        update.where(crb.equal(root.get("codigo"),codigo));

        // execution
        Transaction tr = session.beginTransaction();
        session.createQuery(update).executeUpdate();
        tr.commit();

        System.out.println("peso updated succesfully...");
        System.out.println("Jugador of codigo "+codigo+" weight updated to "+peso);
    }

    // setters and  getters
    public static void setSession(Session session) {
        JugadoresCRUD.session = session;
    }
}

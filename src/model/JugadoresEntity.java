package model;

import java.util.Collection;
import java.util.Objects;

public class JugadoresEntity {
    private int codigo;
    private String nombre;
    private String procedencia;
    private String altura;
    private Integer peso;
    private String posicion;
    private Collection<EstadisticasEntity> estadisticasByCodigo;
    private EquiposEntity equiposByNombreEquipo;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JugadoresEntity that = (JugadoresEntity) o;
        return codigo == that.codigo && Objects.equals(nombre, that.nombre) && Objects.equals(procedencia, that.procedencia) && Objects.equals(altura, that.altura) && Objects.equals(peso, that.peso) && Objects.equals(posicion, that.posicion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, nombre, procedencia, altura, peso, posicion);
    }

    public Collection<EstadisticasEntity> getEstadisticasByCodigo() {
        return estadisticasByCodigo;
    }

    public void setEstadisticasByCodigo(Collection<EstadisticasEntity> estadisticasByCodigo) {
        this.estadisticasByCodigo = estadisticasByCodigo;
    }

    public EquiposEntity getEquiposByNombreEquipo() {
        return equiposByNombreEquipo;
    }

    public void setEquiposByNombreEquipo(EquiposEntity equiposByNombreEquipo) {
        this.equiposByNombreEquipo = equiposByNombreEquipo;
    }

    @Override
    public String toString() {
        return "JugadoresEntity{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", procedencia='" + procedencia + '\'' +
                ", altura='" + altura + '\'' +
                ", peso=" + peso +
                ", posicion='" + posicion + '\'' +
                '}';
    }
}

package model;

import java.util.Collection;
import java.util.Objects;

public class EquiposEntity {
    private String nombre;
    private String ciudad;
    private String conferencia;
    private String division;
    private Collection<JugadoresEntity> jugadoresByNombre;
    private Collection<PartidosEntity> partidosByNombre;
    private Collection<PartidosEntity> partidosByNombre_0;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getConferencia() {
        return conferencia;
    }

    public void setConferencia(String conferencia) {
        this.conferencia = conferencia;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EquiposEntity that = (EquiposEntity) o;
        return Objects.equals(nombre, that.nombre) && Objects.equals(ciudad, that.ciudad) && Objects.equals(conferencia, that.conferencia) && Objects.equals(division, that.division);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, ciudad, conferencia, division);
    }

    public Collection<JugadoresEntity> getJugadoresByNombre() {
        return jugadoresByNombre;
    }

    public void setJugadoresByNombre(Collection<JugadoresEntity> jugadoresByNombre) {
        this.jugadoresByNombre = jugadoresByNombre;
    }

    public Collection<PartidosEntity> getPartidosByNombre() {
        return partidosByNombre;
    }

    public void setPartidosByNombre(Collection<PartidosEntity> partidosByNombre) {
        this.partidosByNombre = partidosByNombre;
    }

    public Collection<PartidosEntity> getPartidosByNombre_0() {
        return partidosByNombre_0;
    }

    public void setPartidosByNombre_0(Collection<PartidosEntity> partidosByNombre_0) {
        this.partidosByNombre_0 = partidosByNombre_0;
    }
}

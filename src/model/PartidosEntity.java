package model;

import java.util.Objects;

public class PartidosEntity {
    private int codigo;
    private Integer puntosLocal;
    private Integer puntosVisitante;
    private String temporada;
    private EquiposEntity equiposByEquipoLocal;
    private EquiposEntity equiposByEquipoVisitante;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Integer getPuntosLocal() {
        return puntosLocal;
    }

    public void setPuntosLocal(Integer puntosLocal) {
        this.puntosLocal = puntosLocal;
    }

    public Integer getPuntosVisitante() {
        return puntosVisitante;
    }

    public void setPuntosVisitante(Integer puntosVisitante) {
        this.puntosVisitante = puntosVisitante;
    }

    public String getTemporada() {
        return temporada;
    }

    public void setTemporada(String temporada) {
        this.temporada = temporada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartidosEntity that = (PartidosEntity) o;
        return codigo == that.codigo && Objects.equals(puntosLocal, that.puntosLocal) && Objects.equals(puntosVisitante, that.puntosVisitante) && Objects.equals(temporada, that.temporada);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, puntosLocal, puntosVisitante, temporada);
    }

    public EquiposEntity getEquiposByEquipoLocal() {
        return equiposByEquipoLocal;
    }

    public void setEquiposByEquipoLocal(EquiposEntity equiposByEquipoLocal) {
        this.equiposByEquipoLocal = equiposByEquipoLocal;
    }

    public EquiposEntity getEquiposByEquipoVisitante() {
        return equiposByEquipoVisitante;
    }

    public void setEquiposByEquipoVisitante(EquiposEntity equiposByEquipoVisitante) {
        this.equiposByEquipoVisitante = equiposByEquipoVisitante;
    }
}

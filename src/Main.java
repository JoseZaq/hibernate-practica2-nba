import CRUD.EquiposCRUD;
import CRUD.EstadisticasCRUD;
import CRUD.JugadoresCRUD;
import model.EquiposEntity;
import model.EstadisticasEntity;
import model.JugadoresEntity;
import org.hibernate.Session;
import utils.Menu;
import utils.Printer;

import java.util.*;

public class Main {
    public static void main(final String[] args) throws Exception {
        final Session session = ConnexioNBA.getSession();
        final String[] options = {"EX1-Add two new estatistics to player 123 if don't exist yet","EX2-Update players weight","EX3-See all player estatistics",
        "EX4-See all equips","EX5-See best points by season"};
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //cruds
        JugadoresCRUD.setSession(session);
        EstadisticasCRUD.setSession(session);
        EquiposCRUD.setSession(session);
        try {
            Menu menu = new Menu(options,scanner){
                @Override
                public void goToMethod(int option) {
                    switch (option){
                        case 1:
                            ex1AddStatisticsToPlayer();
                            break;
                        case 2:
                            ex2UpdatePlayersWeight();
                            break;
                        case 3:
                            ex3ShowPlayerStatistics();
                            break;
                        case 4:
                            ex4ShowEquipDetailList();
                            break;
                        case 5:
                            ex5ShowBestPointsPerSeasonPLayer();
                            break;
                    }
                }
            };
            menu.start();
        } finally {
            session.close();
        }
    }

    /**
     * mostra el jugador amb millor estadísitca de punts per cada temporada.
     */
    private static void ex5ShowBestPointsPerSeasonPLayer() {
        List<Object[]> results = EstadisticasCRUD.getBestPointsPerSeason();
        System.out.println("Mejores puntuadores por temporada");
        for (Object[] result : results) {
            Printer.printLines();
            String temporada = (String) result[0];
            int jugadorCodigo = (int) result[1];
            String jugadorNombre = (String) result[2];
            String jugadorEquipo = (String) result[3];
            double points = (double) result[4];
            System.out.printf("Temporada: %s\n%d,%s,%s\nPuntos: %.2f\n",temporada,jugadorCodigo
            ,jugadorNombre,jugadorEquipo,points);
        }
    }

    /**
     * Crea un mètode que mostri per cada equip la llista dels seus jugadors amb la mitja del punts per partit. El llistat ha
     * d’apareixer ordenat per equip i ha de mostrar també el número d’equips que hi ha. Exemple de sortida del programa:
     */
    private static void ex4ShowEquipDetailList() {
        List<EquiposEntity> allEqs= EquiposCRUD.getAllSortByName();
        int eqsQuantity  = allEqs.size();
        System.out.println("Numero de Equipos: "+eqsQuantity);
        Printer.printLines();
        for (EquiposEntity equipo : allEqs) {
            System.out.println("Equipo: "+equipo.getNombre());
            for (JugadoresEntity players : equipo.getJugadoresByNombre()) {
                Collection<EstadisticasEntity> statistics = players.getEstadisticasByCodigo();
                double averageStatistics = 0;
                for (EstadisticasEntity statistic : statistics) {
                    averageStatistics += statistic.getPuntosPorPartido();
                }
                averageStatistics = averageStatistics / statistics.size();
                System.out.printf("%d, %s: %.2f\n",players.getCodigo(),players.getNombre(), averageStatistics);
            }
            Printer.printLines();
        }
    }

    /**
     * Crea un mètode que admeti un paràmetre. Aquest paràmetre és el codi del jugador, i el mètode ha de mostrar les
     * estadístiques del jugador. Controleu les situacions d’error: si no s’íntrodueix un paràmetre o el paràmetre no és
     * correcte (ha de ser numèric) ha de mostrar un missatge d’error, si el jugador no existeix mostra un missatge que ho
     * indiqui. Prova el mètode mostrant les estadísitiques dels jugadors 227, 43 i 469.
     */
    private static void ex3ShowPlayerStatistics() {
        int[] playerCods = {227, 43, 469};
        List<Collection<EstadisticasEntity>> estatistics = new ArrayList<>();
        for (int codigo : playerCods) {
            try {
                estatistics.add(JugadoresCRUD.get(codigo).getEstadisticasByCodigo());
            }catch (NullPointerException e){
                System.out.println("El jugador con el codigo "+ codigo+" no tiene estadisticas actualmente...");
            }
        }
        if(estatistics.size() != 0)
        for (int i = 0; i < estatistics.size(); i++) {
            EstadisticasEntity _temp= (EstadisticasEntity) estatistics.get(i).toArray()[0];
            JugadoresEntity jugador = _temp.getJugadoresByJugador();
            System.out.printf("\nDatos del Jugador: %s\nNombre: %s\nEquipo:%s\n",
                    jugador.getCodigo(),jugador.getNombre(),jugador.getEquiposByNombreEquipo().getNombre());
            System.out.println("Temporada\tPunts\tAssistències\tTaps\tRebots");
            Printer.printLines();
            for (EstadisticasEntity estad : estatistics.get(i)) {
                System.out.printf("%s\t\t%.2f\t\t%.2f\t\t%.2f\t%.2f\n",
                        estad.getTemporada(),estad.getPuntosPorPartido(),estad.getAsistenciasPorPartido(),
                        estad.getTaponesPorPartido(),estad.getRebotesPorPartido());
            }
            Printer.printLines();
            System.out.printf("Numero de registros: %d\n",estatistics.get(i).size());
            Printer.printLines();
        }
    }

    /**
     * actualitza el pes del jugadors. Pasarem per paràmetres el codi del jugador i el nou pes.
     */
    private static void ex2UpdatePlayersWeight() {
        JugadoresCRUD.updateWeight(101,240);
        JugadoresCRUD.updateWeight(251,200);
        JugadoresCRUD.updateWeight(353,300);
        JugadoresCRUD.updateWeight(561,290);
        JugadoresCRUD.updateWeight(407,263);
    }


    private static void ex1AddStatisticsToPlayer() {
        /**
         * 05/06 7 5
         * 06/07 10 3
         */
        JugadoresEntity jugador123 = JugadoresCRUD.get(123);

        EstadisticasEntity est1 = new EstadisticasEntity();
        est1.setTemporada("05/06");
        est1.setPuntosPorPartido(7.0);
        est1.setRebotesPorPartido(5.0);
        est1.setAsistenciasPorPartido(0.0);
        est1.setJugadoresByJugador(jugador123);
        est1.setTaponesPorPartido(0.0);

        EstadisticasEntity est2 = new EstadisticasEntity();
        est2.setTemporada("06/07");
        est2.setPuntosPorPartido(10.0);
        est2.setRebotesPorPartido(3.0);
        est2.setAsistenciasPorPartido(0.0);
        est2.setJugadoresByJugador(jugador123);
        est2.setTaponesPorPartido(0.0);

        EstadisticasCRUD.insertEstadistica(est1);
        EstadisticasCRUD.insertEstadistica(est2);

    }
}